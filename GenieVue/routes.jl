using Genie.Router

route("/") do
  serve_static_file("welcome.html")
end

route("/firstscript")do
  serve_static_file("firstScript.html")
end
